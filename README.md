## Why this repo?
After completing the course **Karate DSL: API Automation and Performance from Zero to Hero** [source](https://www.udemy.com/course/karate-dsl-api-automation-and-performance-from-zero-to-hero/), I decided to make this repo to have a place to store everything I find valid about Karate DSL. It will also serve as a refresh if needed and as a way to measure my progress in using this tool to write tests and custom reports.

## Karate DSL and Gatling
A repository containing my Karate DSL setup (download, install and config on Linux), some of my testscripts as well as
reports related to my API automation and Gatling performance testing

## Some Pros and cons of Karate DSL
The Pros of Karate:
- Easy to start with Java, with little to no previous programmer background to write tests
- tests are plain text and written based on Gherkin / Cucumber standard
- has native JSON support and very powerful JSON validations
- supports multi-thread parallel execution, which can be a time-saver for e2e and integration tests 
- Java is always running in the background
- supports both Java and Javascript to build a library of re-usable functions or the re-use of custom Java code
- has detailed reporting and logs
- integration with Gatling for Performance testing

Some of the cons of Karate:
- uses own Karate scripting language
- no Intellisense support in IDE, so mistakes will not be noticed
- difficult to find mistakes in the code when script is run

## More info on the official Karate Github
Karate: Test Automation Made Simple: [source](https://github.com/intuit/karate)

## Karate Karate vs REST-Assured detailed comparison 
View on Google Docs: [source](https://docs.google.com/document/d/1ETTrdMVcBXaPjdKY-_67zCWBsi2Ctc5DIQUIfr02H7A/edit)

## Development environment for Karate DSL
Required minimum to run Karate tests:
- Java 8 or higher
- Apache Maven: how to install Maven: [source](https://www.baeldung.com/install-maven-on-windows-linux-mac)
- Git 
- Yarn
- Postman

## Verify if both Java and Maven are installed correctly
- `java --version`
- `mvn -v`

## Also needed is an IDE
- Visual Studio Code, for instance
- VS Code plugins: Cucumber (Gherkin) Full Support, Karate Runner and Java Extension Pack
  
## Note
`pending`will be updated on a later date with information in my own words and / or my own scripts. 
No course data will be copy pasted in here
 
## Set up a Project
Pending

## Setup Karate Framework 
Pending

## API Automation
- **GET request**: pending example 
- **URL and Path**: pending
- **Assertions**: pending
- **POST request**: pending
- **Runner config and tags**: pending
- **DELETE request**: pending
- **Calling other Feature**: pending
- **Environment Variables**: pending
- **Embedded and Multi-Line expressions**: pending
- **Assertions**: pending
- **Fuzzy Matching**: pending
- **Schema validation**: pending
- **Test-Data Generator**: pending
- **Data-driven Scenario**: pending
- **Reading other files**: pending
- **Mistakes and Debugging**: pending

## Advanced Features
- **Before and After Hooks**: pending
- **Parallel Execution**: pending
- **Cucumber Reporter**: pending
- **Conditional Logic**: pending
- **Retry and Sleep**: pending
- **JSON transforms**: pending
- **Type conversion**: pending
- **Connect to SQL Database**: pending
- **Karate in Docker Container**: pending

## Performance Testing with Gatling
- **Gatling setup**: pending
- **First simulation**: pending
- **Karate protocol**: pending
- **User Think TIme**: pending
- **Simulation setup**: pending
- **Feeder from file and custom feeder**: pending
- **Name resolver**: pending
- **Dispatcher config**: pending

